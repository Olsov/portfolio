<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Content@index');
Route::get('/getshirtmodel','Content@getModel');
Route::get('/getproducts','Content@getProducts');
Route::post('/order','Order@sendOrder');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

