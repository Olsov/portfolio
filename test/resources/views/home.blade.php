<!DOCTYPE html>
<html lang="ru">
    <head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>конструктор формы Joma (Nike)</title>
        <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
        <link href="css/slick.css" rel="stylesheet">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <link href="css/slick-theme.css" rel="stylesheet">
        <link href="css/styles.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="top-header">
                <div class="container">
                    <a href=""><i class="fa fa-phone" aria-hidden="true"></i> +7 (8452) 55-55-55</a>
                </div>
            </div>
            <div class="bottom-header">
                <div class="container">
                    <h2>конструктор формы Joma (Nike)</h2>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ul>
                        <li><a href="">Конструктор</a></li>
                        <li><a href="">Доставка </a></li>
                        <li><a href="">Оплата</a></li>
                        <li><a href="">Контакты</a></li>
                        <li><a href="">Интернет-магазин</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="main-content">
            <div class="slogan">
                Будь в форме для мировых рекордов
            </div>
            <div class="container">
                
                <div class="order-block">
                    <h3>Закажите форму в 3 шага</h3>
                    <div class="type">
                        <button class="adults" data-type='1'>Взрослая</button>
                        <button class="children" data-type='0'>Детская</button>
                    </div>
                </div>
                <div class="select-line  hidden">
                    <h4 class="title">1 шаг - Выберите линию формы</h4>
                    <div class="product-list">
                        <div class="inner">
                            <a href="">
                                <span class="img-block"><img src="img/tshirt1.png" alt=""></span>
                                <h5>Campus ii</h5>
                            </a>
                        </div>
                        
                    </div>
                </div> 
                <div class="select-color-block hidden">
                    <h4 class="title">2 шаг - выберите элемент и цвет формы</h4>
                    <div class="additional-choose active shirt">
                        <div class="features" data-type='shirt'>
                            <input type="checkbox" class="checkbox" id="thirt" checked>
                            <label for="thirt">Футболка</label>
                            <div class="varieties">
                                <button class="short-sleeve" data-type='0'>Короткий рукав</button>
                                <button class="long-sleeve" data-type='1'>Длинный рукав</button>
                            </div>
                            <div class="price"><span>2 000 </span><strong>руб</strong></div>
                        </div>
                        <div class="slider-product">
                            <div class="slider-good">
                                <a href="img/tshirt1.png" class="item"  data-thumb="tshirt1.png">
                                    <img src="img/tshirt1.png" alt="">
                                </a>
                                <a href="img/tshirt2.png" class="item"  data-thumb="tshirt2.png">
                                    <img src="img/tshirt2.png" alt="">
                                </a>
                                <a href="img/tshirt3.png" class="item"  data-thumb="tshirt3.png">
                                    <img src="img/tshirt3.png" alt="">
                                </a>
                                <a href="img/tshirt4.png" class="item"  data-thumb="tshirt4.png">
                                    <img src="img/tshirt4.png" alt="">
                                </a>
                                <a href="img/tshirt5.png" class="item"  data-thumb="tshirt5.png">
                                    <img src="img/tshirt5.png" alt="">
                                </a>
                                <a href="img/tshirt6.png" class="item"  data-thumb="tshirt6.png">
                                    <img src="img/tshirt6.png" alt="">
                                </a>
                                 <a href="img/tshirt7.png" class="item"  data-thumb="tshirt7.png">
                                    <img src="img/tshirt7.png" alt="">
                                </a>
                                 <a href="img/tshirt8.png" class="item"  data-thumb="tshirt8.png">
                                    <img src="img/tshirt8.png" alt="">
                                </a>
                                <a href="img/tshirt1.png" class="item"  data-thumb="tshirt1.png">
                                    <img src="img/tshirt1.png" alt="">
                                </a>
                                <a href="img/tshirt2.png" class="item"  data-thumb="tshirt2.png">
                                    <img src="img/tshirt2.png" alt="">
                                </a>
                                <a href="img/tshirt3.png" class="item"  data-thumb="tshirt3.png">
                                    <img src="img/tshirt3.png" alt="">
                                </a>
                                <a href="img/tshirt4.png" class="item"  data-thumb="tshirt4.png">
                                    <img src="img/tshirt4.png" alt="">
                                </a>
                                <a href="img/tshirt5.png" class="item"  data-thumb="tshirt5.png">
                                    <img src="img/tshirt5.png" alt="">
                                </a>
                                <a href="img/tshirt6.png" class="item"  data-thumb="tshirt6.png">
                                    <img src="img/tshirt6.png" alt="">
                                </a>
                                 <a href="img/tshirt7.png" class="item"  data-thumb="tshirt7.png">
                                    <img src="img/tshirt7.png" alt="">
                                </a>
                                <a href="img/tshirt8.png" class="item"  data-thumb="tshirt8.png">
                                    <img src="img/tshirt8.png" alt="">
                                </a>
                                <a href="img/tshirt1.png" class="item"  data-thumb="tshirt1.png">
                                    <img src="img/tshirt1.png" alt="">
                                </a>
                                <a href="img/tshirt2.png" class="item"  data-thumb="tshirt2.png">
                                    <img src="img/tshirt2.png" alt="">
                                </a>
                                <a href="img/tshirt3.png" class="item"  data-thumb="tshirt3.png">
                                    <img src="img/tshirt3.png" alt="">
                                </a>
                                <a href="img/tshirt4.png" class="item"  data-thumb="tshirt4.png">
                                    <img src="img/tshirt4.png" alt="">
                                </a>
                                <a href="img/tshirt5.png" class="item"  data-thumb="tshirt5.png">
                                    <img src="img/tshirt5.png" alt="">
                                </a>
                                <a href="img/tshirt6.png" class="item"  data-thumb="tshirt6.png">
                                    <img src="img/tshirt6.png" alt="">
                                </a>
                                 <a href="img/tshirt7.png" class="item"  data-thumb="tshirt7.png">
                                    <img src="img/tshirt7.png" alt="">
                                </a>
                                 <a href="img/tshirt8.png" class="item"  data-thumb="tshirt8.png">
                                    <img src="img/tshirt8.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="additional-choose shorts active">
                        <div class="features" data-type='shorts' >
                            <input type="checkbox" class="checkbox" checked id="shorts">
                            <label for="shorts">Шорты</label>
                            <div class="varieties">
                                <button class="">Eurocopa</button>
                                <button class="">Nobel</button>
                                <button>TOKIO II</button>
                                <button>Toledo</button>
                            </div>
                            <div class="price"><span>1 000 </span><strong>руб</strong></div>
                        </div>
                        <div class="slider-product">
                            <div class="slider-good">
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                <a href="img/short2.png" class="item"  data-thumb="short2.png">
                                    <img src="img/short2.png" alt="">
                                </a>
                                
                            </div>
                        </div>
                    </div>
                    <div class="additional-choose leg active">
                        <div class="features" data-type='leg'>
                            <input type="checkbox" class="checkbox" id="leg" checked>
                            <label for="leg">Гетры</label>
                            <div class="varieties">
                                <button class="">Eurocopa</button>
                                <button class="">Nobel</button>
                                <button>TOKIO II</button>
                                <button>Toledo</button>
                            </div>
                            <div class="price"><span>1 000 </span><strong>руб</strong></div>
                        </div>
                        <div class="slider-product">
                            <div class="slider-good">
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>

                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                                <a href="img/leg.png" class="item"  data-thumb="leg.png">
                                    <img src="img/leg.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="conlude-block">
                        <div class="conclude-sum">
                            <p>Итого за комплект: <span>3 200 </span>руб.</p>
                        </div>  
                        <a href="" class="go-order">Перейти к оформлению заказа</a>  
                    </div>
                </div>
                <div class="pick-size-block hidden">
                    <h4 class="title">3 шаг - подберите размеры и заполните количество</h4>
                    
                    <div class="wrap shirt_table">
                        <div class="name-сol">
                            <h5>Наименование</h5>
                            <div class="img-block">
                                <a href="img/tshirt8.png"><img src="img/tshirt8.png" width="140" height="184" alt=""></a>
                            </div>
                            <div class="name">Футболка CREW II с коротким рукавом</div>
                            <div class="articul">Артикул: 100014.011</div>
                        </div>
                        <div class="size-col">
                            <h5>Размеры</h5>
                            <div class="description">
                                <div class="tabs">
                                    <div class="tabs-block "><a href="#">Детские</a></div>
                                    <div class="tabs-block tab-active"><a href="#">Взрослые</a></div>
                                    
                                </div>
                                <div class="tabs-text">
                                    <div class="block-tabs-text ">
                                        <table>
                                            <tr>
                                                <th></th>
                                                <th>6</th>
                                                <th>8</th>
                                                <th>10</th>
                                                <th>12</th>
                                                <th>14</th>
                                                <th>16</th>
                                                
                                            </tr>
                                            <tr>
                                                <td>Размеры</td>
                                                <td>30-32</td>
                                                <td>32-34</td>
                                                <td>34-36</td>
                                                <td>36-38</td>
                                                <td>38-40</td>
                                                <td>40-42</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Рост</td>
                                                <td>118-128</td>
                                                <td>128-138</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Количество</td>
                                               <td><input type="number"  data-type='child' data-size='6' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='8' placeholder="0"></td>
                                                <td><input type="number" data-type='child'   data-size='10' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='12' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='14' placeholder="0"></td>
                                                <td><input type="number"  data-type='child'  data-size='16' placeholder="0"></td>
                                                
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="block-tabs-text active">
                                        <table>
                                            <tr>
                                                <th></th>
                                                <th>S</th>
                                                <th>M</th>
                                                <th>L</th>
                                                <th>XL</th>
                                                <th>XXL</th>
                                                <th>XXXL</th>
                                                <th>XXXXL</th>
                                            </tr>
                                            <tr>
                                                <td>Размеры</td>
                                                <td>42-44</td>
                                                <td>46-48</td>
                                                <td>50-52</td>
                                                <td>54-56</td>
                                                <td>60-62</td>
                                                <td>60-62</td>
                                                <td>60-62</td>
                                            </tr>
                                            <tr>
                                                <td>Рост</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                            </tr>
                                            <tr>
                                                <td>Количество</td>
                                            <td><input type="number" data-type='adult' data-size='S' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='M' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='L' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XL' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XXL' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XXXL' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XXXXL' placeholder="0"></td>
                                            </tr>
                                        </table>
                                    </div>    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="sum-col">
                            <h5>Сумма</h5>

                              
                            
                          
                        </div>
                    </div>
                    <div class="wrap short_table">
                        <div class="name-сol">
                            
                            <div class="img-block">
                                <a href="img/short2.png"><img src="img/short2.png" width="129" height="165" alt=""></a>
                            </div>
                            <div class="name">Футболка CREW II с коротким рукавом</div>
                            <div class="articul">Артикул: 100014.011</div>
                        </div>
                        <div class="size-col">
                            
                            <div class="description">
                                <div class="tabs">
                                    <div class="tabs-block "><a href="#">Детские</a></div>
                                    <div class="tabs-block tab-active"><a href="#">Взрослые</a></div>
                                    
                                </div>
                                <div class="tabs-text">
                                    <div class="block-tabs-text ">
                                        <table>
                                            <tr>
                                                <th></th>
                                                <th>6</th>
                                                <th>8</th>
                                                <th>10</th>
                                                <th>12</th>
                                                <th>14</th>
                                                <th>16</th>
                                                
                                            </tr>
                                            <tr>
                                                <td>Размеры</td>
                                                <td>30-32</td>
                                                <td>32-34</td>
                                                <td>34-36</td>
                                                <td>36-38</td>
                                                <td>38-40</td>
                                                <td>40-42</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Рост</td>
                                                <td>118-128</td>
                                                <td>128-138</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Количество</td>
                                         <td><input type="number"  data-type='child' data-size='6' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='8' placeholder="0"></td>
                                                <td><input type="number" data-type='child'   data-size='10' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='12' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='14' placeholder="0"></td>
                                                <td><input type="number"  data-type='child'  data-size='16' placeholder="0"></td>
                                                
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="block-tabs-text active">
                                        <table>
                                            <tr>
                                                <th></th>
                                                <th>S</th>
                                                <th>M</th>
                                                <th>L</th>
                                                <th>XL</th>
                                                <th>XXL</th>
                                                <th>XXXL</th>
                                                <th>XXXXL</th>
                                            </tr>
                                            <tr>
                                                <td>Размеры</td>
                                                <td>42-44</td>
                                                <td>46-48</td>
                                                <td>50-52</td>
                                                <td>54-56</td>
                                                <td>60-62</td>
                                                <td>60-62</td>
                                                <td>60-62</td>
                                            </tr>
                                            <tr>
                                                <td>Рост</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                            </tr>
                                            <tr>
                                                <td>Количество</td>
                                           <td><input type="number" data-type='adult' data-size='S' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='M' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='L' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XL' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XXL' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XXXL' placeholder="0"></td>
                                            <td><input type="number" data-type='adult' data-size='XXXXL' placeholder="0"></td>
                                            </tr>
                                        </table>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="sum-col"> 

                        </div>
                    </div>
                     <div class="wrap leg_table">
                        <div class="name-сol">
                            
                            <div class="img-block">
                                <a href="img/leg2.png"><img src="img/leg2.png" width="92" height="167" alt=""></a>
                            </div>
                            <div class="name">Футболка CREW II с коротким рукавом</div>
                            <div class="articul">Артикул: 100014.011</div>
                        </div>
                        <div class="size-col">
                            
                            <div class="description">
                                <div class="tabs">
                                    <div class="tabs-block "><a href="#">Детские</a></div>
                                    <div class="tabs-block tab-active"><a href="#">Взрослые</a></div>
                                    
                                </div>
                                <div class="tabs-text">
                                    <div class="block-tabs-text ">
                                        <table>
                                            <tr>
                                                <th></th>
                                                <th>6</th>
                                                <th>8</th>
                                                <th>10</th>
                                                <th>12</th>
                                                <th>14</th>
                                                <th>16</th>
                                                
                                            </tr>
                                            <tr>
                                                <td>Размеры</td>
                                                <td>30-32</td>
                                                <td>32-34</td>
                                                <td>34-36</td>
                                                <td>36-38</td>
                                                <td>38-40</td>
                                                <td>40-42</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Рост</td>
                                                <td>118-128</td>
                                                <td>128-138</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                <td>138-146</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Количество</td>
                                                <td><input type="number"  data-type='child' data-size='6' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='8' placeholder="0"></td>
                                                <td><input type="number" data-type='child'   data-size='10' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='12' placeholder="0"></td>
                                                <td><input type="number" data-type='child'  data-size='14' placeholder="0"></td>
                                                <td><input type="number"  data-type='child'  data-size='16' placeholder="0"></td>
                                                
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="block-tabs-text active">
                                        <table>
                                            <tr>
                                                <th></th>
                                                <th>S</th>
                                                <th>M</th>
                                                <th>L</th>
                                                <th>XL</th>
                                                <th>XXL</th>
                                                <th>XXXL</th>
                                                <th>XXXXL</th>
                                            </tr>
                                            <tr>
                                                <td>Размеры</td>
                                                <td>42-44</td>
                                                <td>46-48</td>
                                                <td>50-52</td>
                                                <td>54-56</td>
                                                <td>60-62</td>
                                                <td>60-62</td>
                                                <td>60-62</td>
                                            </tr>
                                            <tr>
                                                <td>Рост</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                                <td>до 168</td>
                                            </tr>
                                            <tr>
                                                <td>Количество</td>
                                                <td><input type="number" data-type='adult' data-size='S' placeholder="0"></td>
                                                <td><input type="number" data-type='adult' data-size='M' placeholder="0"></td>
                                                <td><input type="number" data-type='adult' data-size='L' placeholder="0"></td>
                                                <td><input type="number" data-type='adult' data-size='XL' placeholder="0"></td>
                                                <td><input type="number" data-type='adult' data-size='XXL' placeholder="0"></td>
                                                <td><input type="number" data-type='adult' data-size='XXXL' placeholder="0"></td>
                                                <td><input type="number" data-type='adult' data-size='XXXXL' placeholder="0"></td>
                                            </tr>
                                        </table>
                                    </div>    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="sum-col">
                            
                              
                            
                          
                        </div>
                    </div>
                </div>
                <div class="form-order hidden" >
                    <form action="">
                     {{ csrf_field() }}
                        <div class="wrap">
                            <div class="wrap-input">
                                <input type="text" id="name" name="name" required="required" />
                                <label for="name">Имя</label>
                            </div>
                            <div class="wrap-input">
                                <input type="text" id="phone" name="phone" required="required" />
                                <label for="phone">Телефон</label>
                            </div>
                            <div class="wrap-input">
                                <input type="text" id="email" name="email" required="required" />
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <textarea name="dop-info"  placeholder="Дополнительная информация">
                            
                        </textarea>
                        <span class="sum-order">Сумма вашего заказа: <strong>4 800 </strong>руб.</span>
                        <input type="submit" value="Оформить заказ" class="to-order">
                    </form>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="wrap">
                    <address>Адрес: <span>Саратов, Радищева, 22Б</span></address>
                    <div class="center-col">
                        <p>Электронная почта: <a href="mailto:mail@tl-sport.ru">mail@tl-sport.ru</a></p>
                        <p>Разработано в PremierStudio</p>
                    </div>
                    <div class="tel-block">Телефоны: <a href="tel:+78452 55-55-55">+7 (8452) 55-55-55</a></div>
                </div>
            </div>
        </footer>
      <script src="https://cdn.jsdelivr.net/jquery/2.2.1/jquery.min.js"></script>
      <script src="js/jquery.fancybox.min.js">      </script>
      <script src="js/slick.js"></script>
      <script src="js/scripts.js"></script>
    </body>
</html>
