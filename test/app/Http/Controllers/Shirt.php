<? 
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class Shirt extends Controller
{
  public function getShirtsByModelId($id,$with_hands = -1)
  {
  	if($with_hands == -1 ){

	 $shirt = DB::table('shirts')->where('model_id','=',$id)->select('id','name','price','img','code','type')->get(); 
  	}else{
  		 $shirt = DB::table('shirts')
       ->where('with_hands',$with_hands)->where('model_id',$id)
       ->select('id','name','price','img','code','type')
       ->get(); 
  	}

 	$i=0;
 	$data = array();
 	foreach ($shirt as $sh) {
 		$data[$i]['id']=$sh->id;
 		$data[$i]['name']=$sh->name;
 		$data[$i]['price']= $sh->price;
 		$data[$i]['img']= strpos($sh->img, '://')?$sh->img:asset('storage/'.$sh->img);
    $data[$i]['code']=$sh->code;
    $data[$i]['type'] =$sh->type;
 		$i++;
 	}

 	return $data;

  }
  public function getAllShirts(){
  	
  	$shirt = DB::table('shirts')->select('id','name','price','img')->get(); 
  	$i=0;
 	$data = array();
 	foreach ($shirt as $sh) {

 		$data[$i]['id']=$sh->id;
 		$data[$i]['name']=$sh->name;
 		$data[$i]['price']= $sh->price;
 		$data[$i]['img']= strpos($sh->img, '://')?$sh->img:asset('storage/'.$sh->img);
    $data[$i]['code']=$sh->code;
 		$i++;
 	}

 	return $data;
  }

}