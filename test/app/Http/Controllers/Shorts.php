<? 
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class Shorts extends Controller
{
  public function getShortsByModelId($id)
  {
	 $shirt = DB::table('shorts')->where('model_id',$id)->select('id','name','price','img','code','type')->get(); 


 	$i=0;
 	$data = array();
 	foreach ($shirt as $sh) {

 		$data[$i]['id']=$sh->id;
 		$data[$i]['name']=$sh->name;
 		$data[$i]['price']= $sh->price;
 		$data[$i]['img']= strpos($sh->img, '://')?$sh->img:asset('storage/'.$sh->img);
    $data[$i]['code']=$sh->code;
    $data[$i]['type'] = $sh->type;
 		$i++;
 	}

 	return $data;

  }

   public function getAllShorts(){
  	$shirt = DB::table('shorts')->select('id','name','price','img','code','type')->get(); 
  	$i=0;
 	$data = array();
 	foreach ($shirt as $sh) {

 		$data[$i]['id']=$sh->id;
 		$data[$i]['name']=$sh->name;
 		$data[$i]['price']= $sh->price;
 		$data[$i]['img']= strpos($sh->img, '://')?$sh->img:asset('storage/'.$sh->img);
    $data[$i]['code']=$sh->code;
    $data[$i]['type'] = $sh->type;
 		$i++;
 	}

 	return $data;
  }

    public function getModels($human){
  	$array = DB::table('model_shorts')->where('type',$human)->select('id','model_name')->get();
  	$i=0;
 	$data = array();
 	foreach ($array as $element) {

 		$data[$i]['id']=$element->id;
 		$data[$i]['name']=$element->model_name;

 		$i++;
 	}
 	return $data;
  }
 
}