<?
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Shorts as Shorts;
use App\Http\Controllers\Model as Model;
use App\Http\Controllers\Shirt as Shirt;
use App\Http\Controllers\Gaiter as Gaiter;
use DB;

class Content extends Controller
{
  public function index(){
    return view('home');
  }

  public function getModel(Request $request)
  {	
  	$model = new Model();
 	return $model->getModelsById($request->query('type'));

  }
  public function getProducts(Request $request){
  	$type = $request->query('type');
  	$id = $request->query('id');
  	$data = array();
  	$with_hands = $request->query('with_hands')!==null ? $request->query('with_hands') : -1 ;
  	$data['shirts'] = array();
  	$data['shorts'] = array();
  	$data['gaiters']=array();
  	$data['model_shorts'] = array();
  	$data['model_gaiter'] = array();
  	switch ($type) {
  		case 'shirt' :
  		 $class = new Shirt();

  		 $data['shirts'] = $class->getShirtsByModelId($id,$with_hands);
       return $data;
   			break;
  		case 'shorts':
  		$class = new Shorts();
  		$data['shorts'] = $class->getShortsByModelId($id);
  		break;
  		case 'gaiters':
  		$class  = new Gaiter();
  		$data['gaiters'] = $class->getGaitersByModelId($id);
  		break;
  		case 'all':
  		$class = new Shirt();
  		$data['shirts'] = $class->getShirtsByModelId($id);
  		$class = new Shorts();
  		$data['model_shorts'] = $class->getModels($request->query('human'));
  		$model_short = array();
  	foreach ($data['model_shorts'] as $short) {
  		$data_new = $class->getShortsByModelId($short['id']);
  		$data['shorts']= array_merge(	$data['shorts'],$data_new);
  	}
  		$class  = new Gaiter();
  		$data['model_gaiter']=$class->getModels($request->query('human'));
  		foreach ($data['model_gaiter']as $model_gaiter) {
  				$data_new = $class->getGaitersByModelId($model_gaiter['id']);
  				$data['gaiters']= array_merge($data['gaiters'],$data_new);
  		}
  		break;
  		default:

  			break;
  	}
  	return $data;
  }


}