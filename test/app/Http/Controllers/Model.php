<?
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class Model extends Controller
{
  public function getModelsById($type)
  {
 	$models = 	DB::table('model_shirts')->where("type",$type)->select('id','model_name','model_img','type')->get();
 	$i=0;
 	$data = array();
 	foreach ($models as $model) {
 		$data[$i]['id']=$model->id;
 		$data[$i]['name']=$model->model_name;
 		$data[$i]['img']=strpos($model->model_img, '://')?$model->model_img:asset('storage/'.$model->model_img);
 		$i++;
 	}
 	return $data;

  }
}