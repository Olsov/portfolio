<?
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;

class Order extends Controller
{
  public function sendOrder(Request $request){
    if($request->ajax()){
      //для будущего макета письма html 
      $html = '';
        $order = Input::get('order');
        $leg = Input::get('leg');
        $short = Input::get('short');
        $shirt = Input::get('shirt');
        $html = view('email.order_data', ['order' => $order,'leg'=>$leg,'leg_sizes'=>$leg['sizes'][0],'short_sizes'=>$short['sizes'][0],'shirt_sizes'=>$shirt['sizes'][0],'shirt'=>$shirt,"short"=>$short]);

//       Mail::send('email.order', array('order' => $order,'leg'=>$leg,'leg_sizes'=>$leg['sizes'][0],'short_sizes'=>$short['sizes'][0],'shirt_sizes'=>$shirt['sizes'][0],'shirt'=>$shirt,"short"=>$short), function($message)
// {
//     $message->to('scare_mouse@mail.ru','тест' )->subject('Привет!');
// });
DB::table('orders')->insert(
    [ 'name' => $order['name'], 
      'email'=>$order['email'],
      'phone'=>$order['phone'],
      'comment'=>$order['text'],
      'sum'=>$order['sum'],
      'order_details'=>$html,
    ]
);

    }else{
      echo 'ошибка';
    }
  }
}