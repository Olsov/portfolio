$(function() {
    if ($('.slider-good')) {
        $('.slider-good').slick({
            arrows: true,
            dots: true,
            customPaging : function(slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                return '<a><img src="'+thumb+'"></a>';
            }
        });
    }
     $(".slider-product .item").fancybox({
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'speedIn'   : 600,
        'speedOut'    : 200,
        'overlayShow' : false
      });
    $(".name-сol a").fancybox({
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'speedIn'   : 600,
        'speedOut'    : 200,
        'overlayShow' : false
      });
    $(".tabs-block").click(function(e) {
        e.preventDefault();

        var activeClass = 'tab-active';
        if (!$(this).hasClass(activeClass) && !$(this).hasClass('inactive')) {
            var indexTab = $(".tabs .tabs-block").index(this);
            $(this).closest('.description').find(".block-tabs-text").hide();
            
                $(".tabs-text .block-tabs-text").eq(indexTab).show();
           
            $(this).closest('.description').find(".tabs-block").removeClass(activeClass);
            $(this).addClass(activeClass);
        }

    });
    
    $('.additional-choose input:checkbox').change(function(){
        if($(this).is(":checked")) {
            $(this).closest('.additional-choose').addClass("active");
        } else {
            $(this).closest('.additional-choose').removeClass("active");
        }
    });
});
function makeWorkingSlide(){
     if ($('body .slider-good')) {
        $('.slider-good').slick({
            arrows: true,
            dots: true,
            customPaging : function(slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                var price = $(slider.$slides[i]).data('price');
                return '<a><img data-price="'+price+'" src="'+thumb+'"></a>';
            }
        });
    }
     $("body .slider-product .item").fancybox({
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'speedIn'   : 600,
        'speedOut'    : 200,
        'overlayShow' : false
      });
    $("body .name-сol a").fancybox({
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'speedIn'   : 600,
        'speedOut'    : 200,
        'overlayShow' : false
      });
    $("body .tabs-block").click(function(e) {
        e.preventDefault();
        var activeClass = 'tab-active';
        if (!$(this).hasClass(activeClass) && !$(this).hasClass('inactive')) {
            var indexTab = $(".tabs .tabs-block").index(this);
            $(this).closest('.description').find(".block-tabs-text").hide();
            
                $(".tabs-text .block-tabs-text").eq(indexTab).show();
           
            $(this).closest('.description').find(".tabs-block").removeClass(activeClass);
            $(this).addClass(activeClass);
        }

    });
    
    $('.additional-choose input:checkbox').change(function(){
        if($(this).is(":checked")) {
            $(this).closest('.additional-choose').addClass("active");
        } else {
            $(this).closest('.additional-choose').removeClass("active");
        }
    });
}

//функция перещета цены комплекта
function getTotal(price1 = 0 ,price2 = 0,price3 = 0){
$('.conclude-sum span').text(Number(price1)+Number(price2)+Number(price3)+' ');
}
function getTotalForOrder(){
    var short_price = Number($('.short_table').data('price'));
    var shirt_price  = Number($('.shirt_table').data('price'));
    var leg_price = Number($('.leg_table').data('price'));
    var short_total = 0;
    var shirt_total = 0;
    var leg_total = 0;
    if(!$(".short_table").hasClass('inactive')){
          $('.short_table input').each(function(index, el) {
            short_total+=Number($(el).val())*short_price;
        });
      }
       if(!$(".shirt_table").hasClass('inactive')){
          $('.shirt_table input').each(function(index, el) {
            shirt_total+=Number($(el).val())*shirt_price;
        });
      }
       if(!$(".leg_table").hasClass('inactive')){
          $('.leg_table input').each(function(index, el) {
            leg_total+=Number($(el).val())*leg_price;
        });
      }
var total = leg_total+shirt_total+short_total;
      $('form .sum-order strong').text(total);

}
// script for ajax
var price_shirt=0;
var name_shirt ='';
var price_shorts = 0;
var name_shorts = '';
var price_leg = 0;
var name_leg = '';

  var human = '';
$('body').on('click', '.type button', function(event) {
    event.preventDefault();
    /* Act on the event */
    $('.type button').removeClass('active');
    $(this).addClass('active');
   var type = $(this).data('type');
   human = type;
    $.ajax({
        url: 'getshirtmodel',
        type: 'GET',
        dataType: 'json',
        data: {'type': type},
    })
    .done(function(data) {

         $('.product-list').html('');
         if($('.select-line').hasClass('hidden')){
            $('.select-line').slideDown('300', function() {

    $('html, body').animate({
        scrollTop: $(".select-line").offset().top
    }, 1000);
    $('.select-line').removeClass('hidden')
            });
         }
      var links ='';
        $.each(data, function(i,item) {

       links+='<div class="inner" data-id="'+item.id+'">'+'<a href=""><span class="img-block"><img src="'+item.img+'" alt=""></span><h5>'+item.name+'</h5></a></div>';
    });
        $('.product-list').append(links);
        console.log("success");
    })
    .fail(function(error) {
 $('body').html(error['responseText']);
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
    
});

$('body').on('click', '.product-list .inner', function(event) {
    event.preventDefault();
    var id =$(this).data('id');
    var type ='all';

    $.ajax({
            url: 'getproducts',
            type: 'GET',
            dataType: 'json',
            data: {'id': id,'type':type,'human':human},
        })
        .done(function(data) {
                //for shirts
                $('.shirt .varieties button').data('id',id);
                $('.shirt .slider-product').html('');
                var elements = '<div class="slider-good">';
                $.each(data['shirts'], function(i,item) {
                  elements+='<a href="'+item.img+'" class="item" data-code="'+item.code+'" data-name="'+item.name+'" data-price="'+item.price+'" data-type="'+item.type+'" data-thumb="'+item.img+'"><img src="'+item.img+'" alt=""></a>';
                });
                elements+='</div>';
                  $('.shirt .slider-product').append(elements);
                $('.shorts .varieties').html('');
// for shots
  $('.shorts .slider-product').html('');
  var elements = '<div class="slider-good">';
                $.each(data['shorts'], function(i,item) {
                     elements+='<a href="'+item.img+'" class="item" data-type="'+item.type+'"  data-code="'+item.code+'" data-name="'+item.name+'" data-price="'+item.price+'" data-thumb="'+item.img+'"><img src="'+item.img+'" alt=""></a>';
                 });
                elements+='</div>';

                  $('.shorts .slider-product').append(elements);
                  // for gaiters
              
                   $('.leg .slider-product').html('');
  var elements = '<div class="slider-good">';
                $.each(data['gaiters'], function(i,item) {

                    elements+='<a href="'+item.img+'" class="item" data-type="'+item.type+'"  data-code="'+item.code+'" data-name="'+item.name+'" data-price="'+item.price+'" data-thumb="'+item.img+'"><img src="'+item.img+'" alt=""></a>';
                 });
                elements+='</div>';

                  $('.leg .slider-product').append(elements);


  
            
                //получаем модели шорт
                elements = '';
                 $.each(data['model_shorts'], function(i,item) {
                    elements+='<button data-id="'+item.id+'">'+item.name+'</button>';

                 });
                 $('.shorts .varieties').append(elements);

                 //модели гетр 
                    $('.leg .varieties').html('');
                //получаем модели шорт
                elements = '';
                 $.each(data['model_gaiter'], function(i,item) {
                    elements+='<button data-id="'+item.id+'">'+item.name+'</button>';

                 });
                 $('.leg .varieties').append(elements);
                 //считаем сумму по всем вещам
 
//yakor
                     if($('.select-color-block').hasClass('hidden')){
            $('.select-color-block').slideDown('300', function() {

    $('html, body').animate({
        scrollTop: $(".select-color-block").offset().top
    }, 1000);

    $('.select-color-block').removeClass('hidden')
            });
         }

          
  
                    makeWorkingSlide();

                     price_shirt = $('.shirt .slick-active').data('price');
             price_short = $('.shorts .slick-active').data('price');
             price_leg = $('.leg .slick-active').data('price');

                $('.shirt').find('.price span:nth-child(1)').text(price_shirt);
                $('.shorts').find('.price span:nth-child(1)').text(price_short);
                $('.leg').find('.price span:nth-child(1)').text(price_short);


                            getTotal(price_shirt, price_short,  price_short);
            console.log("success");
        })
        .fail(function(error) {
            $('body').html(JSON.stringify(error['responseText']));
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
            /* Act on the event */
});
//changing type of shirt 

$('body').on('click', '.shirt .varieties button', function(event) {
    $('.shirt .varieties button').removeClass('active');
        $(this).addClass('active');
        var with_hands = $(this).data('type');
        var id  = $(this).data('id');
        var type = 'shirt';
        $.ajax({
            url: 'getproducts',
            type: 'GET',
            dataType: 'JSON',
            data: {'with_hands':with_hands,'type':type,'id':id},
        })
        .done(function(data) {
                $('.shirt .slider-product').html('');
                var elements = '<div class="slider-good">';
                $.each(data['shirts'], function(i,item) {
                    elements+='<a href="'+item.img+'" class="item" data-type="'+item.type+'"  data-code="'+item.code+'" data-name="'+item.name+'" data-price="'+item.price+'" data-thumb="'+item.img+'"><img src="'+item.img+'" alt=""></a>';
                });
                elements+='</div>';
                  $('.shirt .slider-product').append(elements);
             makeWorkingSlide();
            console.log("success");
        })
        .fail(function(error) {
            $('body').html(error['responseText']);
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        

//     /* Act on the event */
});

//for shorts
$('body').on('click', '.shorts .varieties button', function(event) {
    $('.shorts .varieties button').removeClass('active');
        $(this).addClass('active');
        var id  = $(this).data('id');
        var type = 'shorts';
        $.ajax({
            url: 'getproducts',
            type: 'GET',
            dataType: 'JSON',
            data: {'type':type,'id':id},
        })
        .done(function(data) {

              $('.shorts .slider-product').html('');
  var elements = '<div class="slider-good">';
                $.each(data['shorts'], function(i,item) {
                    elements+='<a href="'+item.img+'" class="item" data-type="'+item.type+'"  data-code="'+item.code+'" data-name="'+item.name+'" data-price="'+item.price+'" data-thumb="'+item.img+'"><img src="'+item.img+'" alt=""></a>';
                });
                elements+='</div>';

                  $('.shorts .slider-product').append(elements);
                   $('.shorts .slider-good').slick({
            arrows: true,
            dots: true,
            customPaging : function(slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                return '<a><img src="'+thumb+'"></a>';
            }
        });
            console.log("success");
        })
        .fail(function(error) {
            $('body').html(error['responseText']);
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        

//     /* Act on the event */
});
// выбираем класс гетров 

$('body').on('click', '.leg .varieties button', function(event) {
    $('.leg .varieties button').removeClass('active');
        $(this).addClass('active');
        var id  = $(this).data('id');
        var type = 'gaiters';
        $.ajax({
            url: 'getproducts',
            type: 'GET',
            dataType: 'JSON',
            data: {'type':type,'id':id},
        })

            
        .done(function(data) {
              $('.leg .slider-product').html('');
            var elements = '<div class="slider-good">';
                $.each(data['gaiters'], function(i,item) {
                     elements+='<a href="'+item.img+'" class="item" data-type="'+item.type+'" data-code="'+item.code+'" data-name="'+item.name+'" data-price="'+item.price+'" data-thumb="'+item.img+'"><img src="'+item.img+'" alt=""></a>';
                 });
                elements+='</div>';

                  $('.leg .slider-product').append(elements);
                   $('.leg .slider-good').slick({
            arrows: true,
            dots: true,
            customPaging : function(slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                return '<a><img src="'+thumb+'"></a>';
            }
        });
            console.log("success");
        })
        .fail(function(error) {
            $('body').html(error['responseText']);
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        

//     /* Act on the event */
});

//меняем модель шортов 
$('body').on('click', '.go-order', function(event) {
        event.preventDefault();
    //yakor
                     if($('.pick-size-block').hasClass('hidden')){
            $('.pick-size-block').slideDown('300', function() {

    $('html, body').animate({
        scrollTop: $(".pick-size-block").offset().top
    }, 1000);
    $('.form-order').slideDown('300', function() {
    });
     $('.form-order').removeClass('hidden');
    $('.pick-size-block').removeClass('hidden')
            });
         }


var shirt=  new Object();
shirt.name = $('.shirt .slick-active').data('name');
shirt.image = $('.shirt .slick-active').attr('href');
shirt.price = $('.shirt .slick-active').data('price');
shirt.type = $('.shirt .slick-active').data('type');
shirt.code = $('.shirt .slick-active').data('code');

var short = new Object();
short.name = $('.shorts .slick-active').data('name');
short.price = $('.shorts .slick-active').data('price');
short.type = $('.shorts .slick-active').data('type');
short.image = $('.shorts .slick-active').attr('href');
short.code = $('.shorts .slick-active').data('code');
var leg = new Object();
leg.name = $('.leg .slick-active').data('name');
leg.price = $('.leg .slick-active').data('price');
leg.image = $('.leg .slick-active').attr('href');
leg.type = $('.leg .slick-active').data('type');
leg.code = $('.leg .slick-active').data('code');
//makin shirt
$(".shirt_table .img-block a").attr('href',shirt.image);
$(".shirt_table .img-block img").attr('src',shirt.image);
$(".shirt_table .name").text(shirt.name);
$(".shirt_table .articul").text('Артикул: '+shirt.code);
$('.shirt_table').data('price',shirt.price);
if(shirt.type ==2){
$('.shirt_table .tabs-block:nth-child(1)').addClass('inactive');
}
if(shirt.type == 1){
$('.shirt_table .tabs-block:nth-child(2)').addClass('inactive');
$('.shirt_table .tabs-block:nth-child(2)').removeClass('tab-active');
$('.shirt_table .tabs-block:nth-child(1)').addClass('tab-active');
}

// making short 
$(".short_table .img-block a").attr('href',short.image);
$(".short_table .img-block img").attr('src',short.image);
$(".short_table .name").text(short.name);
$(".short_table .articul").text('Артикул: '+short.code);
$('.short_table').data('price',short.price);
if(short.type == 2){
$('.short_table .tabs-block:nth-child(1)').addClass('inactive');
}
if(short.type == 1){
$('.short_table .tabs-block:nth-child(2)').addClass('inactive');
$('.short_table .tabs-block:nth-child(2)').removeClass('tab-active');
$('.short_table .tabs-block:nth-child(1)').addClass('tab-active');
}
// making leg
// making short 
$(".leg_table .img-block a").attr('href',leg.image);
$(".leg_table .img-block img").attr('src',leg.image);
$(".leg_table .name").text(leg.name);
$(".leg_table .articul").text('Артикул: '+leg.code);
$('.leg_table').data('price',leg.price);
if(leg.type == 2){
$('.leg_table .tabs-block:nth-child(1)').addClass('inactive');
}
if(leg.type == 1){
$('.leg_table .tabs-block:nth-child(2)').addClass('inactive');
$('.leg_table .tabs-block:nth-child(2)').removeClass('tab-active');
$('.leg_table .tabs-block:nth-child(1)').addClass('tab-active');
}
         getTotalForOrder();
    return false;

});

$('body').on('change', '.wrap input', function(event) {
    event.preventDefault();
    var type = $(this).data('size');
    var price_once = Number($(this).closest('.wrap').data('price'));
    var child_price = 0;
    var adult_price = 0;
    var i = 0; 
    var count_child = 0;
    var count_adult = 0;
$.each( $(this).closest('.wrap').find('input'), function(index, val) {
    if(i<6){
        if($(this).val() > 0)
        count_child += Number($(this).val());
    }else{
        if($(this).val() > 0)
        count_adult+=Number($(this).val());
      /* iterate through array or object */
    }
    i++;
 });
child_price = count_child*price_once;
adult_price = count_adult*price_once;

    if ( child_price == 0 ){
        $(this).closest('.wrap').find('.sum-child').slideUp('300',function(){
            $(this).detach();

        });

    }else{
            if( $(this).closest('.wrap').find('.sum-child').length > 0){
                $(this).closest('.wrap').find('.sum-child .quanity span').text(count_child);
                $(this).closest('.wrap').find('.sum-child .sum-adults-price span').text(child_price);
            }else{
                var html ='<div class="sum-child"><h6>Детские:</h6><div class="wrap-price"><div class="price-point">'+ $(this).closest('.wrap').data("price")+' руб. </div><strong>х</strong><div class="quanity"> <span>'+count_child+'</span> шт.</div><button class="delete"><i class="fa fa-window-close-o" aria-hidden="true"></i></button><div class="sum-adults-price"><span>'+child_price+'</span> руб.</div></div>';
                $(this).closest('.wrap').find('.sum-col').append(html);
            }
    }

    if ( adult_price == 0 ){
        $(this).closest('.wrap').find('.sum-adults').slideUp('300',function(){
            $(this).detach();

        });

    }else{
            if( $(this).closest('.wrap').find('.sum-adults').length > 0){

                $(this).closest('.wrap').find('.sum-adults .quanity span').text(count_adult);
                $(this).closest('.wrap').find('.sum-adults .sum-adults-price span').text(adult_price);
            }else{
                var html ='<div class="sum-adults"><h6>Взрослые:</h6><div class="wrap-price"><div class="price-point">'+$(this).closest('.wrap').data("price")+' руб. </div><strong>х</strong><div class="quanity"> <span>'+count_adult+'</span> шт.</div><button class="delete"><i class="fa fa-window-close-o" aria-hidden="true"></i></button><div class="sum-adults-price"><span>'+adult_price+'</span> руб.</div></div>';
                $(this).closest('.wrap').find('.sum-col').append(html);
            }
    }
          getTotalForOrder();
});

//удаляем количество 
$('body').on('click', '.sum-adults .fa-window-close-o', function(event) {
    event.preventDefault();
    $(this).closest('.wrap').find('input[data-type="adult"]').val(0);
    $(this).closest('.sum-adults').detach();
    /* Act on the event */
});
$('body').on('click', '.sum-child .fa-window-close-o', function(event) {
    event.preventDefault();
    $(this).closest('.wrap').find('input[data-type="child"]').val(0);
    $(this).closest('.sum-child').detach();
    /* Act on the event */
});
// 
$('body').on('click', '.additional-choose li', function(event) {
    var price = $(this).find('img').data('price');
    $(this).closest('.additional-choose').find('.price span:nth-child(1)').text(price);
     getTotal($('.shirt').find('.price span:nth-child(1)').text(), $('.shorts').find('.price span:nth-child(1)').text(),  $('.leg').find('.price span:nth-child(1)').text());
         
    /* Act on the event */
});
$('body').on('click', '.additional-choose .a', function(event) {

    $(this).closest('.additional-choose').find('.price span:nth-child(1)').text($(this).data('price'));
     getTotal($('.shirt').find('.price span:nth-child(1)').text(), $('.shorts').find('.price span:nth-child(1)').text(),  $('.leg').find('.price span:nth-child(1)').text());
         
    /* Act on the event */
});
$('body').on('click', '.slick-arrow', function(event) {
    /* Act on the event */
             price_shirt = $('.shirt .slick-active').data('price');
             price_short = $('.shorts .slick-active').data('price');
             price_leg = $('.leg .slick-active').data('price');

                $('.shirt').find('.price span:nth-child(1)').text(price_shirt);
                $('.shorts').find('.price span:nth-child(1)').text(price_short);
                $('.leg').find('.price span:nth-child(1)').text(price_leg);
                 getTotal($('.shirt').find('.price span:nth-child(1)').text(), $('.shorts').find('.price span:nth-child(1)').text(),  $('.leg').find('.price span:nth-child(1)').text());
         

});
    $("body").on('mouseover','.item', function() {
        $(this).fancybox({
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'speedIn'   : 600,
        'speedOut'    : 200,
        'overlayShow' : false
      });
        return false;
    });
    $('body').on('click', '.select-color-block input[type="checkbox"]', function(event) {
      if($(this).prop('checked')){
          var type = $(this).closest('.features').data('type');
        switch(type) {
            case 'shirt':
                   $('.shirt_table').removeClass('inactive');
                break;
            case 'short':
                     $('.short_table').removeClass('inactive');
                break;
            case 'leg':
            $('.leg_table').removeClass('inactive');
            break;
        }

        getTotal($('.shirt').find('.price span:nth-child(1)').text(), $('.shorts').find('.price span:nth-child(1)').text(),  $('.leg').find('.price span:nth-child(1)').text());
          
      }else{
        var price  = Number($(this).closest('.features').find('.price span:nth-child(1)').text());
        var type = $(this).closest('.features').data('type');
        switch(type) {
            case 'shirt':
                   $('.shirt_table').addClass('inactive');
                break;
            case 'short':
                     $('.short_table').addClass('inactive');
                break;
            case 'leg':
            $('.leg_table').addClass('inactive');
            break;
        }
        var total_price = Number($('.conclude-sum span').text());
        total_price = total_price - price;
        $('.conclude-sum span').text(total_price+' ');
      }
               getTotalForOrder();
    });
  
//оформляем заказ
   $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('body').on('click', '.to-order', function(event) {

    event.preventDefault();
    //имя
    var order = new Object();
order['name'] = $('form input[name="name"]').val();
//телефон
order['phone'] = $('form input[name="phone"]').val();
//емеил
order['email'] =$('form input[name="email"]').val();
//отзыв
order['text'] = $('form textarea[name="dop-info"]').val();
//сумма
order['sum'] = $('form .sum-order strong').text();
// получаем информацию об футболках 
var shirt = new Object();
shirt.name = $('.shirt_table .name').text();
shirt.code = $('.shirt_table .articul').text();
shirt.price = $('.shirt_table').data('price');
shirt_size = new Object();
shirt.sizes= [];
var i = 0;
$('.shirt_table input').each(function(index, el) {
    var value  = $(el).val();
    var size = $(el).data('size');
    shirt_size[size]=value;
    shirt.sizes.push(shirt_size);
    i++;
});
//получаем информацию об шортах
var short = new Object();
short.name = $('.short_table .name').text();
short.code = $('.short_table .articul').text();
short.price = $('.short_table').data('price');
short_size = new Object();
short.sizes= [];
var i = 0;
$('.short_table input').each(function(index, el) {
    var value  = $(el).val();
    var size = $(el).data('size');
    short_size[size]=value;
    short.sizes.push(short_size);
    i++;
});
//получаем информацию об ногах
var leg = new Object();
leg.name = $('.leg_table .name').text();
leg.code = $('.leg_table .articul').text();
leg.price = $('.leg_table').data('price');
leg_size = new Object();
leg.sizes= [];
var i = 0;
$('.leg_table input').each(function(index, el) {
    var value  = $(el).val();
    var size = $(el).data('size');
    leg_size[size]=value;
    leg.sizes.push(leg_size);
    i++;
});
$.ajax({
    url: 'order',
    type: 'post',
    // dataType: 'json',
    data: {'order':order,'shirt':shirt,'short':short,'leg':leg},
})
.done(function(data) {
    // alert(data['response']);
   alert(data);
    // $('body').html(data);
    console.log("success");
})
.fail(function(error) {
$('body').append(error['responseText']);
    console.log("error");
})
.always(function() {
    console.log("complete");
});

    /* Act on the event */
});
// 